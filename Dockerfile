FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive
ENV IDF_VERSION=v4.4.4
ENV USCXML_VERSION 121887fe5775b1b7a2da39aca416e52caa47cc59
ENV PATH /usr/local/bin:$PATH

RUN echo "deb http://deb.debian.org/debian bullseye-backports main" > /etc/apt/sources.list.d/backports.list
RUN apt -y -q update && apt -y -q upgrade
RUN apt -y -q install sudo make cmake build-essential proot crossbuild-essential-arm64 \
        bison flex device-tree-compiler u-boot-tools binfmt-support qemu-user-static \
        mtools gcc-aarch64-linux-gnu pkg-config libssl-dev fakeroot rsync libconfuse-dev \
        bc dosfstools cpio zlib1g-dev kmod binutils debootstrap parted mount e2fsprogs \
        parted coreutils unzip zip wget libgpiod-dev libudev-dev libsystemd-dev git bzip2 \
        python3-pip xxd python3-sphinx default-jre-headless libcurl4-openssl-dev libssl-dev \
        python-is-python3 libevent-dev dfu-util libusb-1.0-0

RUN apt -y -q install -t bullseye-backports genimage

RUN pip install --pre azure-cli --extra-index-url https://azurecliprod.blob.core.windows.net/edge

COPY patch/esp-idf/*.patch /patch/esp-idf/
COPY patch/uscxml/*.patch /patch/uscxml/

# IDF install 
RUN wget -q https://dl.espressif.com/github_assets/espressif/esp-idf/releases/download/${IDF_VERSION}/esp-idf-${IDF_VERSION}.zip
RUN unzip -q esp-idf-${IDF_VERSION}.zip -d /usr/local && rm esp-idf-${IDF_VERSION}.zip
# let the pipeline do the patching if IDF

RUN for patch in $(ls /patch/esp-idf/*.patch); do \
    echo Applying IDF $patch; \
        if patch -g0 -p1 -E -d /usr/local/esp-idf-${IDF_VERSION} -i $patch -t -s -N; then \
           echo "Applied IDF patch $patch" ;\
        else \
           echo "Error $? in IDF patch $patch" ;\
           exit 1; \
        fi; \
    done;

RUN /usr/local/esp-idf-${IDF_VERSION}/install.sh esp32s3 > /dev/null

# build and install uSCXML 
RUN wget -q https://github.com/tklab-tud/uscxml/archive/${USCXML_VERSION}.zip && \
        unzip -q ${USCXML_VERSION}.zip -d src

RUN for patch in $(ls /patch/uscxml/*.patch); do \
        echo Applying uSCXML $patch; \
        if patch -g0 -p1 -E -d src/uscxml-${USCXML_VERSION} -i $patch -t -N; then \
           echo "Applied uSCXML patch $patch" ;\
        else \
           echo "Error $? in uSCXML patch $patch" ;\
           exit 1; \
        fi; \
    done;

RUN cmake -DCMAKE_CXX_FLAGS="-include functional" -S src/uscxml-${USCXML_VERSION} -B build \
        -DBUILD_BINDING_JAVA=OFF -DBUILD_BINDING_LUA=OFF -DBUILD_BINDING_CSHARP=OFF -DBUILD_BINDING_PYTHON=OFF && \
        cmake --build build && cmake --build build --target install && mv /build/deps/xerces-c/lib/* /usr/local/lib/ && \
        rm -r ${USCXML_VERSION}.zip src/uscxml-${USCXML_VERSION} 

RUN apt clean
